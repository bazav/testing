/*
Заполняем поле "Срок" в проекте , при изменеии значения в поле "Подтип"
*/
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import java.sql.Timestamp

def issue = event.issue

Calendar calendar = Calendar.getInstance()
def cfm = ComponentAccessor.getCustomFieldManager()
def issueManager = ComponentAccessor.getIssueManager()
def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def priority = issue.getPriority().getName()
def type = issue.getIssueType().getName()
String subtype
subtype = event?.getChangeLog()?.getRelated("ChildChangeItem")?.find { it.field in ["Подтип"] }?.newstring
log.warn("значение в поле подтип: " + subtype)
int i = 0

////////////////////////////////////////////////////////////////////////////////////////////////////////

if (type == "Отчет об оценке" && priority == "Высокий" && subtype == "жилая недвижимость (квартиры)"){
	calendar.setTime(issue.getCreated())
    
for (int j=0 ; j < 3; j++){
    if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            ++i;
        }
    calendar.add(Calendar.DATE,1)
}
		calendar.add(Calendar.DATE,i-1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)

    if(calendar.get(Calendar.DAY_OF_WEEK) == 1){ //Суббота
    	calendar.add(Calendar.DATE,1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}
	if(calendar.get(Calendar.DAY_OF_WEEK) == 7){ // Воскресенье
    	calendar.add(Calendar.DATE,2)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}   
	  
}

if (type == "Отчет об оценке" && priority == "Средний" && subtype == "жилая недвижимость (квартиры)"){
	calendar.setTime(issue.getCreated())

for (int j=0 ; j < 7; j++){
    if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            ++i;
        }
    calendar.add(Calendar.DATE,1)
}
		calendar.add(Calendar.DATE,i-1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)

    if(calendar.get(Calendar.DAY_OF_WEEK) == 1){ //Суббота
    	calendar.add(Calendar.DATE,1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}
	if(calendar.get(Calendar.DAY_OF_WEEK) == 7){ // Воскресенье
    	calendar.add(Calendar.DATE,2)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}   
	  
}

///////////////////////////////////////////////////////////////////////////////////////////////////////

if (type == "Отчет об оценке" && priority == "Высокий" && subtype == "жилая недвижимость (жилые дома)"){
	calendar.setTime(issue.getCreated())

for (int j=0 ; j < 5; j++){
    if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            ++i;
        }
    calendar.add(Calendar.DATE,1)
}
		calendar.add(Calendar.DATE,i-1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)

    if(calendar.get(Calendar.DAY_OF_WEEK) == 1){ //Суббота
    	calendar.add(Calendar.DATE,1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}
	if(calendar.get(Calendar.DAY_OF_WEEK) == 7){ // Воскресенье
    	calendar.add(Calendar.DATE,2)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}   
	  
}

if (type == "Отчет об оценке" && priority == "Средний" && subtype == "жилая недвижимость (жилые дома)"){
	calendar.setTime(issue.getCreated())

for (int j=0 ; j < 10; j++){
    if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            ++i;
        }
    calendar.add(Calendar.DATE,1)
}
		calendar.add(Calendar.DATE,i-1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)

    if(calendar.get(Calendar.DAY_OF_WEEK) == 1){ //Суббота
    	calendar.add(Calendar.DATE,1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}
	if(calendar.get(Calendar.DAY_OF_WEEK) == 7){ // Воскресенье
    	calendar.add(Calendar.DATE,2)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}   
	  
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if (type == "Отчет об оценке" && priority == "Высокий" && subtype == "движимое имущество (транспортные средства)"){
	calendar.setTime(issue.getCreated())

for (int j=0 ; j < 3; j++){
    if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            ++i;
        }
    calendar.add(Calendar.DATE,1)
}
		calendar.add(Calendar.DATE,i-1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)

    if(calendar.get(Calendar.DAY_OF_WEEK) == 1){ //Суббота
    	calendar.add(Calendar.DATE,1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}
	if(calendar.get(Calendar.DAY_OF_WEEK) == 7){ // Воскресенье
    	calendar.add(Calendar.DATE,2)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}   
	  
}

if (type == "Отчет об оценке" && priority == "Средний" && subtype == "движимое имущество (транспортные средства)"){
	calendar.setTime(issue.getCreated())

for (int j=0 ; j < 7; j++){
    if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            ++i;
        }
    calendar.add(Calendar.DATE,1)
}
		calendar.add(Calendar.DATE,i-1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)

    if(calendar.get(Calendar.DAY_OF_WEEK) == 1){ //Суббота
    	calendar.add(Calendar.DATE,1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}
	if(calendar.get(Calendar.DAY_OF_WEEK) == 7){ // Воскресенье
    	calendar.add(Calendar.DATE,2)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}   
	  
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if (type == "Отчет об оценке" && priority == "Высокий" && subtype == "движимое имущество (оборудование)"){
	calendar.setTime(issue.getCreated())

for (int j=0 ; j < 7; j++){
    if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            ++i;
        }
    calendar.add(Calendar.DATE,1)
}
		calendar.add(Calendar.DATE,i-1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)

    if(calendar.get(Calendar.DAY_OF_WEEK) == 1){ //Суббота
    	calendar.add(Calendar.DATE,1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}
	if(calendar.get(Calendar.DAY_OF_WEEK) == 7){ // Воскресенье
    	calendar.add(Calendar.DATE,2)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}   
	  
}

if (type == "Отчет об оценке" && priority == "Средний" && subtype == "движимое имущество (оборудование)"){
	calendar.setTime(issue.getCreated())

for (int j=0 ; j < 14; j++){
    if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            ++i;
        }
    calendar.add(Calendar.DATE,1)
}
		calendar.add(Calendar.DATE,i-1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)

    if(calendar.get(Calendar.DAY_OF_WEEK) == 1){ //Суббота
    	calendar.add(Calendar.DATE,1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}
	if(calendar.get(Calendar.DAY_OF_WEEK) == 7){ // Воскресенье
    	calendar.add(Calendar.DATE,2)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}   
	  
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

if (type == "Отчет об оценке" && priority == "Высокий" && subtype == "земельные участки"){
	calendar.setTime(issue.getCreated())

for (int j=0 ; j < 5; j++){
    if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            ++i;
        }
    calendar.add(Calendar.DATE,1)
}
		calendar.add(Calendar.DATE,i-1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)

    if(calendar.get(Calendar.DAY_OF_WEEK) == 1){ //Суббота
    	calendar.add(Calendar.DATE,1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}
	if(calendar.get(Calendar.DAY_OF_WEEK) == 7){ // Воскресенье
    	calendar.add(Calendar.DATE,2)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}   
	  
}

if (type == "Отчет об оценке" && priority == "Средний" && subtype == "земельные участки"){
	calendar.setTime(issue.getCreated())

for (int j=0 ; j < 10; j++){
    if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            ++i;
        }
    calendar.add(Calendar.DATE,1)
}
		calendar.add(Calendar.DATE,i-1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)

    if(calendar.get(Calendar.DAY_OF_WEEK) == 1){ //Суббота
    	calendar.add(Calendar.DATE,1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}
	if(calendar.get(Calendar.DAY_OF_WEEK) == 7){ // Воскресенье
    	calendar.add(Calendar.DATE,2)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}   
	  
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

if (type == "Отчет об оценке" && priority == "Высокий" && subtype == "нежилая недвижимость (встроенные помещения)"){
	calendar.setTime(issue.getCreated())

for (int j=0 ; j < 5; j++){
    if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            ++i;
        }
    calendar.add(Calendar.DATE,1)
}
		calendar.add(Calendar.DATE,i-1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)

    if(calendar.get(Calendar.DAY_OF_WEEK) == 1){ //Суббота
    	calendar.add(Calendar.DATE,1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}
	if(calendar.get(Calendar.DAY_OF_WEEK) == 7){ // Воскресенье
    	calendar.add(Calendar.DATE,2)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}   
	  
}

if (type == "Отчет об оценке" && priority == "Средний" && subtype == "нежилая недвижимость (встроенные помещения)"){
	calendar.setTime(issue.getCreated())

for (int j=0 ; j < 10; j++){
    if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            ++i;
        }
    calendar.add(Calendar.DATE,1)
}
		calendar.add(Calendar.DATE,i-1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)

    if(calendar.get(Calendar.DAY_OF_WEEK) == 1){ //Суббота
    	calendar.add(Calendar.DATE,1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}
	if(calendar.get(Calendar.DAY_OF_WEEK) == 7){ // Воскресенье
    	calendar.add(Calendar.DATE,2)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}   
	  
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

if (type == "Отчет об оценке" && priority == "Высокий" && subtype == "нежилая недвижимость (здания)"){
	calendar.setTime(issue.getCreated())

for (int j=0 ; j < 5; j++){
    if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            ++i;
        }
    calendar.add(Calendar.DATE,1)
}
		calendar.add(Calendar.DATE,i-1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)

    if(calendar.get(Calendar.DAY_OF_WEEK) == 1){ //Суббота
    	calendar.add(Calendar.DATE,1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}
	if(calendar.get(Calendar.DAY_OF_WEEK) == 7){ // Воскресенье
    	calendar.add(Calendar.DATE,2)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}   
	  
}

if (type == "Отчет об оценке" && priority == "Средний" && subtype == "нежилая недвижимость (здания)"){
	calendar.setTime(issue.getCreated())

for (int j=0 ; j < 10; j++){
    if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            ++i;
        }
    calendar.add(Calendar.DATE,1)
}
		calendar.add(Calendar.DATE,i-1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)

    if(calendar.get(Calendar.DAY_OF_WEEK) == 1){ //Суббота
    	calendar.add(Calendar.DATE,1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}
	if(calendar.get(Calendar.DAY_OF_WEEK) == 7){ // Воскресенье
    	calendar.add(Calendar.DATE,2)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}   
	  
}

//////////////////////////////////////////////////////////////////////////////////////////////////

if (type == "Отчет об оценке" && priority == "Высокий" && subtype == "нежилая недвижимость (имущественные комплексы)"){
	calendar.setTime(issue.getCreated())

for (int j=0 ; j < 7; j++){
    if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            ++i;
        }
    calendar.add(Calendar.DATE,1)
}
		calendar.add(Calendar.DATE,i-1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)

    if(calendar.get(Calendar.DAY_OF_WEEK) == 1){ //Суббота
    	calendar.add(Calendar.DATE,1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}
	if(calendar.get(Calendar.DAY_OF_WEEK) == 7){ // Воскресенье
    	calendar.add(Calendar.DATE,2)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}   
	  
}

if (type == "Отчет об оценке" && priority == "Средний" && subtype == "нежилая недвижимость (имущественные комплексы)"){
	calendar.setTime(issue.getCreated())

for (int j=0 ; j < 14; j++){
    if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            ++i;
        }
    calendar.add(Calendar.DATE,1)
}
		calendar.add(Calendar.DATE,i-1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)

    if(calendar.get(Calendar.DAY_OF_WEEK) == 1){ //Суббота
    	calendar.add(Calendar.DATE,1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}
	if(calendar.get(Calendar.DAY_OF_WEEK) == 7){ // Воскресенье
    	calendar.add(Calendar.DATE,2)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}   
	  
}

////////////////////////////////////////////////////////////////////////////////////////////////////

if (type == "Отчет об оценке" && priority == "Высокий" && subtype == "оценка бизнеса"){
	calendar.setTime(issue.getCreated())

for (int j=0 ; j < 8; j++){
    if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            ++i;
        }
    calendar.add(Calendar.DATE,1)
}
		calendar.add(Calendar.DATE,i-1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)

    if(calendar.get(Calendar.DAY_OF_WEEK) == 1){ //Суббота
    	calendar.add(Calendar.DATE,1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}
	if(calendar.get(Calendar.DAY_OF_WEEK) == 7){ // Воскресенье
    	calendar.add(Calendar.DATE,2)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}   
	  
}

if (type == "Отчет об оценке" && priority == "Средний" && subtype == "оценка бизнеса"){
	calendar.setTime(issue.getCreated())

for (int j=0 ; j < 16; j++){
    if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            ++i;
        }
    calendar.add(Calendar.DATE,1)
}
		calendar.add(Calendar.DATE,i-1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)

    if(calendar.get(Calendar.DAY_OF_WEEK) == 1){ //Суббота
    	calendar.add(Calendar.DATE,1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}
	if(calendar.get(Calendar.DAY_OF_WEEK) == 7){ // Воскресенье
    	calendar.add(Calendar.DATE,2)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}   
	  
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

if (type == "Отчет об оценке" && priority == "Высокий" && subtype == "оценка прав требования"){
	calendar.setTime(issue.getCreated())

for (int j=0 ; j < 8; j++){
    if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            ++i;
        }
    calendar.add(Calendar.DATE,1)
}
		calendar.add(Calendar.DATE,i-1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)

    if(calendar.get(Calendar.DAY_OF_WEEK) == 1){ //Суббота
    	calendar.add(Calendar.DATE,1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}
	if(calendar.get(Calendar.DAY_OF_WEEK) == 7){ // Воскресенье
    	calendar.add(Calendar.DATE,2)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}   
	  
}

if (type == "Отчет об оценке" && priority == "Средний" && subtype == "оценка прав требования"){
	calendar.setTime(issue.getCreated())

for (int j=0 ; j < 16; j++){
    if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            ++i;
        }
    calendar.add(Calendar.DATE,1)
}
		calendar.add(Calendar.DATE,i-1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)

    if(calendar.get(Calendar.DAY_OF_WEEK) == 1){ //Суббота
    	calendar.add(Calendar.DATE,1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}
	if(calendar.get(Calendar.DAY_OF_WEEK) == 7){ // Воскресенье
    	calendar.add(Calendar.DATE,2)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}   
	  
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////


if (type == "Отчет об оценке" && priority == "Высокий" && subtype == "иное имущество"){
	calendar.setTime(issue.getCreated())

for (int j=0 ; j < 5; j++){
    if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            ++i;
        }
    calendar.add(Calendar.DATE,1)
}
		calendar.add(Calendar.DATE,i-1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)

    if(calendar.get(Calendar.DAY_OF_WEEK) == 1){ //Суббота
    	calendar.add(Calendar.DATE,1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}
	if(calendar.get(Calendar.DAY_OF_WEEK) == 7){ // Воскресенье
    	calendar.add(Calendar.DATE,2)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}   
	  
}

if (type == "Отчет об оценке" && priority == "Средний" && subtype == "иное имущество"){
	calendar.setTime(issue.getCreated())

for (int j=0 ; j < 10; j++){
    if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            ++i;
        }
    calendar.add(Calendar.DATE,1)
}
		calendar.add(Calendar.DATE,i-1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)

    if(calendar.get(Calendar.DAY_OF_WEEK) == 1){ //Суббота
    	calendar.add(Calendar.DATE,1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}
	if(calendar.get(Calendar.DAY_OF_WEEK) == 7){ // Воскресенье
    	calendar.add(Calendar.DATE,2)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}   
	  
}

////////////////////////////////////////////////////////////////////////////////////////////////

if (type == "Отчет об оценке" && priority == "Высокий" && subtype == "несколько видов имущества"){
	calendar.setTime(issue.getCreated())

for (int j=0 ; j < 8; j++){
    if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            ++i;
        }
    calendar.add(Calendar.DATE,1)
}
		calendar.add(Calendar.DATE,i-1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)

    if(calendar.get(Calendar.DAY_OF_WEEK) == 1){ //Суббота
    	calendar.add(Calendar.DATE,1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}
	if(calendar.get(Calendar.DAY_OF_WEEK) == 7){ // Воскресенье
    	calendar.add(Calendar.DATE,2)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}   
	  
}

if (type == "Отчет об оценке" && priority == "Средний" && subtype == "несколько видов имущества"){
	calendar.setTime(issue.getCreated())

for (int j=0 ; j < 16; j++){
    if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            ++i;
        }
    calendar.add(Calendar.DATE,1)
}
		calendar.add(Calendar.DATE,i-1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)

    if(calendar.get(Calendar.DAY_OF_WEEK) == 1){ //Суббота
    	calendar.add(Calendar.DATE,1)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}
	if(calendar.get(Calendar.DAY_OF_WEEK) == 7){ // Воскресенье
    	calendar.add(Calendar.DATE,2)
		issue.setDueDate(new Timestamp(calendar.getTime().getTime()))    
    	issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
	}   
	  
}
