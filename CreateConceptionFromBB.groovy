package ru.bspb.jira.scripts.validators

import com.opensymphony.workflow.InvalidInputException
import com.atlassian.applinks.api.ApplicationLink
import com.atlassian.applinks.api.ApplicationLinkRequestFactory
import com.atlassian.applinks.api.ApplicationLinkService
import com.atlassian.applinks.api.application.confluence.ConfluenceApplicationType
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.sal.api.component.ComponentLocator
import com.atlassian.sal.api.net.Request
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper

String placeHolderForIssueKey = "JU-572"
String rootPageId = "80183821"
String defaultPageId = "149162261"
String templatePageId = "149162240"
String targetSpaceKey = "concept"

Issue issue = issue // ComponentAccessor.getIssueManager().getIssueByCurrentKey("JU-408")
def currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def confluence = new Confluence()
def userDirectorate = ActiveDirectoryAtributes.getDirectorateFor(currentUser)

String ancestorPageId = defaultPageId
if(userDirectorate?.size() > 1){
    def existedChildren = confluence.getChildrenForRoot(rootPageId)
    def existedPageForDerictorate = existedChildren.find{it["title"].equals(userDirectorate)}

    if(existedPageForDerictorate){
        ancestorPageId = existedPageForDerictorate["id"]
    } else {
        ancestorPageId = confluence.createNewPage(confluence.createPageParamsWith(userDirectorate, targetSpaceKey, rootPageId, null))["id"]
    }
} else {
    ancestorPageId = defaultPageId
}

String template = confluence.getContentForPageId(templatePageId)
template = template.replace(placeHolderForIssueKey, issue.key)
try {
    confluence.createNewPage(confluence.createPageParamsWith("ПК." + [issue.key, issue.summary].join(" "), targetSpaceKey, ancestorPageId, template))
} catch (Exception e){
    throw new InvalidInputException("Уже существует концепция с данным названием.")
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
class ActiveDirectoryAtributes {
    static String getDirectorateFor(def user){
        Class activeDirectoryAttributeManagerClass = ComponentAccessor.getPluginAccessor().getClassLoader().findClass("com.intenso.jira.plugins.admanager.util.ActiveDirectoryAttributeManager")
        def activeDirectoryAttributeManager = ComponentAccessor.getOSGiComponentInstanceOfType(activeDirectoryAttributeManagerClass)
        return activeDirectoryAttributeManager.getUserAttribute(user, 101)
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////
class Confluence {
    private ApplicationLinkRequestFactory authenticatedRequestFactory

    Confluence(){
        ApplicationLink confluenceLink = getPrimaryConfluenceLink()
        assert confluenceLink // must have a working app link set up
        this.authenticatedRequestFactory = confluenceLink.createAuthenticatedRequestFactory()
    }
    private ApplicationLink getPrimaryConfluenceLink() {
        def applicationLinkService = ComponentLocator.getComponent(ApplicationLinkService)
        final ApplicationLink conflLink = applicationLinkService.getPrimaryApplicationLink(ConfluenceApplicationType)
        return conflLink
    }
    
    String getContentForPageId(String pageId){
        return new JsonSlurper().parseText(
            this.authenticatedRequestFactory.createRequest(Request.MethodType.GET, "rest/api/content/"+pageId+"?expand=body.storage").execute()
        )["body"]["storage"]["value"]
    }

    List getChildrenForRoot(String pageId){
        return new JsonSlurper().parseText(
            this.authenticatedRequestFactory.createRequest(Request.MethodType.GET, "rest/api/content/"+pageId+"/child/page?limit=100").execute()
        )["results"] as List
    }

    def createNewPage(def pageParams){
        return new JsonSlurper().parseText(
            this.authenticatedRequestFactory.createRequest(Request.MethodType.POST, "rest/api/content")
            .addHeader("Content-Type", "application/json")
            .setRequestBody(new JsonBuilder(pageParams).toString())
            .execute()
        )
    }
    def createPageParamsWith(String pageTitle, String targetSpaceKey, String ancestorPageId, String contentBody) {
        return [type:"page",
                title:pageTitle,
                space:[key:targetSpaceKey],
                ancestors:[[type:"page",id:ancestorPageId]],
                body:[storage:[value:contentBody,representation:"storage"]]
               ]
    }
}

