<html>
<button type="button" id="getData">Get Data</button>
</html>

let jsonObj =  {
  "fields": {
    "project": {
      "id": "12700"
    },
    "summary": "something's wrong",
    "issuetype": {
      "id": "10100"
    },
    "assignee": {
      "name": "user"
    }
  }
};

let username = "user";
let password = "1234";
let auth = btoa(`${username}:${password}`);

let headers = {
  Accept: "application/json",
  "Content-Type": "application/json",
  Authorization: `Basic ${auth}`
};

const url = "https://jira.ru/rest/api/2/issue";

const element = document.getElementById("getData");
element.addEventListener("click", getData);


async function getData() {
  //console.log(JSON.stringify(jsonObj));
  let response = await fetch(url,{
    method: "POST",
    headers,
    body: JSON.stringify(jsonObj)
  });
  if (response.ok) {
    let data = await response.json();
    console.log(data);
    return data
  } else {
    alert('error', response.status);
  }
}